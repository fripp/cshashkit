// NSObject+CSHash.m
//
// Copyright (c) 2013 Calogero Sanfilippo
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

#import "NSObject+CSHash.h"
#import "CSHashableDescriptor.h"
#import <objc/runtime.h>
#import <pthread.h>


static pthread_mutex_t hashableMutex = PTHREAD_MUTEX_INITIALIZER;

static const char *key = "hashableKey";



void addIvarName(Class class, const char *name){
    
    pthread_mutex_lock(&hashableMutex);
    
    
    if(class_getClassVariable(class, name)){
        
        NSMutableSet *set = objc_getAssociatedObject(class, key);
        
        if (!set){
            set = [NSMutableSet set];
            objc_setAssociatedObject(class, key, set, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    
        CSHashableDescriptor *descriptor = [CSHashableDescriptor hashableIvarWithName:@(name)];
    
        [set addObject:descriptor];
    }
    
    
    pthread_mutex_unlock(&hashableMutex);
    
}

void addPropertyName(Class class, const char *propertyName){
    
    pthread_mutex_lock(&hashableMutex);
    

    if(class_getProperty(class, propertyName)){
        
        NSMutableSet *set = objc_getAssociatedObject(class, key);
        
        if (!set) {
            set = [NSMutableSet set];
            
            objc_setAssociatedObject(class, key, set, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
        }
        
        CSHashableDescriptor *descriptor = [CSHashableDescriptor hashablePropertyWithName:@(propertyName)];
        
        [set addObject:descriptor];
    }
    
    pthread_mutex_unlock(&hashableMutex);

}

@implementation NSObject (CSHash)


- (BOOL) cs_IsEqual:(id)object{
    
    if (self == object)
        return YES;
    else if (!object || ![object isKindOfClass:[self class]])
        return NO;
    return [self cs_objectSpecificIsEqual:object];
    
}



- (NSUInteger) cs_hash{
    @autoreleasepool {
        
        pthread_mutex_lock(&hashableMutex);
        
        NSSet *hashableSet = objc_getAssociatedObject([self class], key);
        
        NSSet *hashableSetCopy = [hashableSet copy];
        
        pthread_mutex_unlock(&hashableMutex);
        
                
        if (hashableSet) {
            return [self cs_filteredHash:hashableSetCopy];
        }else
            return [self cs_fullHash];
        
    }
}




#pragma mark - Private methods

- (BOOL) cs_filteredObjectSpecificIsEqual:(id) object ivarNameSet:(NSSet*) set{
    @autoreleasepool {
        
        
        BOOL areEquals = YES;
        
        for (CSHashableDescriptor *descriptor in set) {
            
            
            if([descriptor isIvar]){
                
                
                Ivar currentIvar = class_getInstanceVariable([self class], [[descriptor name] UTF8String]);
                
                Ivar otherIvar = class_getInstanceVariable([object class], [[descriptor name] UTF8String]);
                
                const char *ivarTypeEncoding = ivar_getTypeEncoding(currentIvar);
                
                
                NSString *typeEncoding = @(ivarTypeEncoding);
                
                
                if ([typeEncoding hasPrefix:@"@"]) {
                    
                    
                    id value = object_getIvar(self, currentIvar);
                    
                    id otherValue = object_getIvar(object, otherIvar);
                    
                    
                    areEquals = [self cs_firstObject:value isEqualTo:otherValue typeEncondig:typeEncoding];
                    
                }else{
                    
                    ptrdiff_t currentPtrDiff = ivar_getOffset(currentIvar);
                    ptrdiff_t otherPtrDiff = ivar_getOffset(otherIvar);
                    
                    void *currentIvarPtr = ((__bridge void*)self) + currentPtrDiff;
                    void *otherIvarPtr = ((__bridge void*)object) + otherPtrDiff;
                    
                    
                    NSValue *currentIvarValue = [NSValue valueWithBytes:currentIvarPtr objCType:ivarTypeEncoding];
                    
                    NSValue *otherIvarValue = [NSValue valueWithBytes:otherIvarPtr objCType:ivarTypeEncoding];
                    
                    
                    if (![currentIvarValue isEqualToValue:otherIvarValue])
                        areEquals = NO;
                    
                    
                }
            }else{
                
                objc_property_t property = class_getProperty(self.class, [[descriptor name] UTF8String]);
                
                NSString *attributes = @(property_getAttributes(property));
                
                NSArray *components = [attributes componentsSeparatedByString:@","];
                
                NSString *type = [components[0] substringFromIndex:1];
                
                id currentValue = [self valueForKey:[descriptor name]];
                id otherValue = [object valueForKey:[descriptor name]];
                
                areEquals = [self cs_firstObject:currentValue isEqualTo:otherValue typeEncondig:type];
                
            }
            
            
            if (!areEquals)
                break;
            
        }
        
        
        return areEquals;
    }
    
}

- (BOOL) cs_firstObject:(id) object isEqualTo:(id) other typeEncondig:(NSString*) typeEncondig{
    BOOL areEquals = YES;
    
    if (!object && !other) {
        areEquals = YES;
        
    }else{
        
        if ([typeEncondig hasPrefix:@"@\"NS"]) {
            
            if ([object isKindOfClass:[NSArray class]]) {
                
                areEquals = [object isEqualToArray:other];
            }else if ([object isKindOfClass:[NSDictionary class]]){
                
                areEquals = [object isEqualToDictionary:other];
            }else if ([object isKindOfClass:NSSet.class]){
                areEquals = [object isEqualToSet:other];
            }else if ([object isKindOfClass:NSString.class]){
                areEquals = [object isEqualToString:other];
            }else if ([object isKindOfClass:NSData.class]){
                areEquals = [object isEqualToData:other];
            }else if ([object isKindOfClass:NSDate.class]){
                areEquals = [object isEqualToDate:other];
            }else if ([object isKindOfClass:NSNumber.class]){
                areEquals = [object isEqualToNumber:other];
            }else if ([object isKindOfClass:NSHashTable.class]){
                areEquals = [object isEqualToHashTable:other];
            }else if ([object isKindOfClass:NSAttributedString.class]){
                areEquals = [object isEqualToAttributedString:other];
            }else if ([object isKindOfClass:NSIndexSet.class]){
                areEquals = [object isEqualToIndexSet:other];
            }else if ([object isKindOfClass:NSTimeZone.class]){
                areEquals = [object isEqualToTimeZone:other];
            }else if ([object isKindOfClass:NSOrderedSet.class]){
                areEquals = [object isEqualToOrderedSet:other];
            }else if ([object isKindOfClass:NSValue.class]){
                areEquals = [object isEqualToValue:other];
            }else{
                areEquals = [object isEqual:other];
            }
            
            
        }else{
            if (![object isEqual:other])
                areEquals = NO;
            
        }
    }
    
    return areEquals;
}

- (BOOL) cs_fullObjectSpecificIsEqual:(id) object{
    @autoreleasepool {
        unsigned int ivarCount = 0;
        
        NSMutableDictionary *otherObjectIvars = [NSMutableDictionary dictionary];
        
        Ivar *ivars = class_copyIvarList([object class], &ivarCount);
        
        
        for (int i = 0; i < ivarCount; i++) {
            Ivar currentIvar = ivars[i];
            
            const char *name = ivar_getName(currentIvar);
            
            NSString *key = [[NSString alloc] initWithUTF8String:name];
            
            otherObjectIvars[key] = [NSValue valueWithBytes:&currentIvar objCType:@encode(Ivar)];
            
        }
        
        if (ivars)
            free(ivars);
        
        ivars = class_copyIvarList([self class], &ivarCount);
        
        BOOL areEquals = YES;
        
        for (int i = 0; i < ivarCount && areEquals; i++) {
            Ivar currentIvar = ivars[i];
            
            const char *ivarTypeEncoding = ivar_getTypeEncoding(currentIvar);
            
            const char *name = ivar_getName(currentIvar);
            
            NSString *typeEncoding = @(ivarTypeEncoding);
            
            NSValue *otherIvarValue = otherObjectIvars[@(name)];
            
            Ivar otherIvar;
            
            [otherIvarValue getValue:&otherIvar];
            
            if ([typeEncoding hasPrefix:@"@"]) {
                
                
                id value = object_getIvar(self, currentIvar);
                
                id otherValue = object_getIvar(object, otherIvar);
                
                areEquals = [self cs_firstObject:value isEqualTo:otherValue typeEncondig:typeEncoding];
                
                
                
            }else{
                
                ptrdiff_t currentPtrDiff = ivar_getOffset(currentIvar);
                ptrdiff_t otherPtrDiff = ivar_getOffset(otherIvar);
                
                void *currentIvarPtr = ((__bridge void*)self) + currentPtrDiff;
                void *otherIvarPtr = ((__bridge void*)object) + otherPtrDiff;
                
                
                NSValue *currentIvarValue = [NSValue valueWithBytes:currentIvarPtr objCType:ivarTypeEncoding];
                
                otherIvarValue = [NSValue valueWithBytes:otherIvarPtr objCType:ivarTypeEncoding];
                
                areEquals = [currentIvarValue isEqualToValue:otherIvarValue];
                
            }
        }
        
        if (ivars)
            free(ivars);
        
        
        return areEquals;
    }
    
}

- (NSUInteger) cs_fullHash{
    @autoreleasepool {
        
        unsigned int count = 0;
        
        NSUInteger result = 1;
        NSUInteger prime = 31;
        
        Ivar *ivars = class_copyIvarList([self class], &count);
        
        
        
        for (int i = 0; i < count; i++) {
            
            Ivar ivar = ivars[i];
            
            
            id value;
            
            value = [self cs_valueForIvar:ivar];
            
            NSUInteger objectHash = (value == nil ? 0 : [value hash]);
            
            result = prime * result + objectHash;
        }
        
        
        
        if (ivars)
            free(ivars);
        
        return result;
    }
}

- (NSUInteger) cs_filteredHash:(NSSet*) descriptorSet{
    
    NSUInteger result = 1;
    NSUInteger prime = 31;
    
    id value = nil;
    
    for (CSHashableDescriptor *descriptor in descriptorSet) {
        
        if ([descriptor isIvar]) {
            
            Ivar ivar = class_getInstanceVariable([self class], [[descriptor name] UTF8String]);
            
            
            value = [self cs_valueForIvar:ivar];
            
        }else{
            
            value = [self valueForKey:[descriptor name]];
            
        }
        
        NSUInteger objectHash = (value == nil ? 0 : [value hash]);
        
        result = prime * result + objectHash;
        
        
    }
    
    return result;
}


- (id)cs_valueForIvar:(Ivar)ivar {
    const char *ivarTypeEncoding = ivar_getTypeEncoding(ivar);
    
    NSString *typeEncoding =@(ivarTypeEncoding);
    
    id value = nil;
    
    if ([typeEncoding hasPrefix:@"@"]) {
        //object
        
        value = object_getIvar(self, ivar);
        
        
    }else{
        
        ptrdiff_t diff = ivar_getOffset(ivar);
        
        void *var = ((__bridge void*)self) + diff;
        
        value = [NSValue valueWithBytes:var objCType:ivarTypeEncoding];
        
    }
    return value;
}


- (BOOL) cs_objectSpecificIsEqual:(id) object{
    
    pthread_mutex_lock(&hashableMutex);
    
    NSSet *hashableSet = objc_getAssociatedObject([self class], key);
    
    NSSet *hashableSetCopy = [hashableSet copy];
    
    pthread_mutex_unlock(&hashableMutex);
    
    if (hashableSet)
        return [self cs_filteredObjectSpecificIsEqual:object ivarNameSet:hashableSetCopy];
    else
        return [self cs_fullObjectSpecificIsEqual:object];
    
}


@end
