//
//  CSHashableDescriptor.h
//  CSHashKit
//
//  Created by feanor on 27/07/13.
//
//

#import <Foundation/Foundation.h>

@interface CSHashableDescriptor : NSObject <NSCopying>

@property (assign, nonatomic, getter = isIvar) BOOL ivar;
@property (assign, nonatomic, getter = isProperty) BOOL property;
@property (copy, nonatomic) NSString *name;

+ (instancetype) hashablePropertyWithName:(NSString*) name;
+ (instancetype) hashableIvarWithName:(NSString*) name;

- (BOOL)isEqual:(id)other;

- (BOOL)isEqualToDescriptor:(CSHashableDescriptor *)descriptor;

- (NSUInteger)hash;

- (id)copyWithZone:(NSZone *)zone;

@end
