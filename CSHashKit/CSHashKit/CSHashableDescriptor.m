//
//  CSHashableDescriptor.m
//  CSHashKit
//
//  Created by feanor on 27/07/13.
//
//

#import "NSObject+CSHash.h"

#import "CSHashableDescriptor.h"




@implementation CSHashableDescriptor

- (id) initPropertyDescriptorWithName:(NSString*) name{
    self = [super init];
    
    if (self) {
        self.name = name;
        self.property = YES;
        self.ivar = NO;
    }
    
    return self;
}

- (id) initIvarDescriptorWithName:(NSString*) name{
    self = [super init];
    
    if (self) {
        self.name = name;
        self.property = NO;
        self.ivar = YES;
    }
    
    return self;
}

+ (instancetype) hashablePropertyWithName:(NSString *)name{
    return [[self alloc] initPropertyDescriptorWithName:name];
}

+ (instancetype) hashableIvarWithName:(NSString *)name{
    return [[self alloc] initIvarDescriptorWithName:name];;
}

- (void) dealloc{
    NSLog(@"Dealloc %@", self);
}

- (BOOL)isEqual:(id)other {
	if (other == self)
		return YES;
	if (!other || ![[other class] isEqual:[self class]])
		return NO;

	return [self isEqualToDescriptor:other];
}

- (BOOL)isEqualToDescriptor:(CSHashableDescriptor *)descriptor {
	if (self == descriptor)
		return YES;
	if (descriptor == nil)
		return NO;
	if (self.ivar != descriptor.ivar)
		return NO;
	if (self.property != descriptor.property)
		return NO;
	if (self.name != descriptor.name && ![self.name isEqualToString:descriptor.name])
		return NO;
	return YES;
}

- (NSUInteger)hash {
	NSUInteger hash = (NSUInteger) self.ivar;
	hash = hash * 31u + self.property;
	hash = hash * 31u + [self.name hash];
	return hash;
}

- (id)copyWithZone:(NSZone *)zone {
	CSHashableDescriptor *copy = [[[self class] allocWithZone:zone] init];

	if (copy != nil) {
		copy.ivar = self.ivar;
		copy.property = self.property;
		copy.name = self.name;
	}

	return copy;
}


@end
