
//
//  Class1.m
//  CSHashKit
//
//  Created by feanor on 24/07/13.
//
//

#import "Class1.h"
#import "NSObject+CSHash.h"

@implementation Class1

- (id) init{
    self = [super init];
    
    if (self) {
        
        HASHABLE(intValue);
        HASHABLE(mutableIvar);
        HASHABLE_PROPERTY(array);
        
    }
    
    return self;
}

- (NSUInteger) hash{
    return [self cs_hash];
}

- (BOOL) isEqual:(id)object{
    return [self cs_IsEqual:object];
}

@end
