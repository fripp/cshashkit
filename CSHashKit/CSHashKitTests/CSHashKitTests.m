//
//  CSHashKitTests.m
//  CSHashKitTests
//
//  Created by feanor on 23/07/13.
//
//

#import "CSHashKitTests.h"
#import "Class1.h"

@implementation CSHashKitTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}


- (void)testExamplePasses
{
    Class1 *c1 = [Class1 new];
    Class1 *c2 = [Class1 new];
    
    c1->intValue = 91;
    c2->intValue = 91;
    
    c1->doubleValue = 33.;
    c2->doubleValue = 3.2;

    c1.val = 123;
    c2.val = 123;
    
    c1->mutableIvar = @"asd";
    c2->mutableIvar = @"asd";
    
    c1.array = @[@1];
    c2.array = @[@1];
    
    STAssertEqualObjects(c1, c2, @"");
    STAssertEquals([c1 hash], [c2 hash], @"");
}


@end
