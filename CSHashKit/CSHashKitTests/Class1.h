//
//  Class1.h
//  CSHashKit
//
//  Created by feanor on 24/07/13.
//
//

#import <Foundation/Foundation.h>

@interface Class1 : NSObject{
    @public
    int intValue;
    double doubleValue;
    NSString *mutableIvar;
}

@property (strong, nonatomic) NSArray *array;
@property (assign) int val;
@property (assign) NSRange range;
@end
